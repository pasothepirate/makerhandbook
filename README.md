---
description: Un Prontuario per Makers.
---
Ciao! Questa è la repository di un piccolo [prontuario per makers](https://pasothepirate.gitlab.io/makerhandbook/it/) e non.

Per ora è molto incompleto quindi se vuoi darmi una mano puoi contribuire.

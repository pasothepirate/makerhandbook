---
description: Un Prontuario per Makers.
---
Ciao!  Questo è un piccolo prontuario per makers e non.

Per ora è molto incompleto quindi se vuoi darmi una mano puoi contribuire
alla [repository Gitlab](https://gitlab.com/pasothepirate/makerhandbook) su cui è hostato.

## Categorie

### Software
- Linguaggi di programmazione
- Strutture di dati
- Version Control System
- Static Site Generator
- Integrated Development Environment
- Computer-Aided Design


### Hardware
- Minuteria
  * Bulloni
  * Rondelle
- Sistema di Trasmissione
- Guide lineari

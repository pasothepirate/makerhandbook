# Summary

* [Introduzione](README.md)

### Software
[software](software/README.md)
- [Linguaggi di programmazione](software/programming-language/README.md)
- [Strutture di dati](software/data-structure/README.md)
  * [JSON](software/data-structure/json.md)
- [Version Control System](software/vcs/README.md)
  * [git](software/vcs/git.md)
- [Static Site Generator](software/ssg/README.md)
- [Integrated Development Environment](software/ide/README.md)
- [Computer-Aided Design](software/cad/README.md)


### Hardware
- [Minuteria](hardware/parts/README.md)
  * [Bulloni](hardware/parts/bolts.md)
  * [Rondelle](hardware/parts/washer.md)
- [Sistema di Trasmissione](hardware/transmission/README.md)
- [Guide lineari](hardware/guide/README.md)


* [Glossario](GLOSSARY.md)

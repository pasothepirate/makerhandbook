## Tipologie di programmi
- [Version Control System](./vcs/README.md)
- [Static Site Generator](./ssg/README.md)
- [Integrated Development Environment](./ide/README.md)
- [Computer-Aided Design](./cad/README.md)

## Altro
- [Linguaggi di Programmazione](./programming-language/README.md)
- [Data Structure](./data-structure/README.md)


## Version control
### Riferimenti
https://en.wikipedia.org/wiki/List_of_version_control_software

### Elenco
git

### Most popular
github
gitlab
bitbucket


## Static Site Generator
### Riferimenti
https://www.staticgen.com/

### Elenco
jekyll
hugo
gitbook

### Most popular
...

## Dynamic Site Generator
